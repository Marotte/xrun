#!/usr/bin/env python3
# setup.py

import sys, os

from cx_Freeze import setup, Executable



build_exe_options = {"path":sys.path+['./src'],
                     "packages": 
                                 ['packaging',
                                  'packaging.version',
                                  'packaging.specifiers',
                                  'packaging.requirements',
                                  'appdirs',
                                  'cffi',
                                  'idna']
                    }

install_exe_options = {"install_dir": '.'}


setup(name = "xrun",
      version = "0.1",
      description = "xrun SSH Client",
      options = {"build_exe": build_exe_options, "install_exe": install_exe_options},
      executables = [Executable(script="src/xrun.py")]
     )

