import sys

class Logger:

    def __init__(self, level):
    
        self.output = sys.stderr
        self.log_level = level
        
    def log(self, level, message):
    
        if level >= self.log_level:
        
            print(message, file=self.output)
            
