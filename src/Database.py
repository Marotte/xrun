import os, sqlite3

import Logger

class Database:

    logger = Logger.Logger(1)

    def __init__(self, dbfile = ':memory:'):
    
        self.connection = sqlite3.connect(dbfile)
        self.cursor     = self.connection.cursor()

    def execute_script(self, script):
        
        self.cursor.executescript(script)
        self.commit()   

    def execute_file(self, sqlfile):

        try:

            script = open(sqlfile, 'r').read()
            self.execute_script(script)
            self.commit()

        except FileNotFoundError as e:
            
            self.logger.log(2, str(e))

    def execute(self, query, values):
        
        return(self.cursor.execute(query, values))
        
    def fetchall(self, query):
    
        return(self.cursor.execute(query).fetchall())
        
    def commit(self):
        
        self.connection.commit()    
        

