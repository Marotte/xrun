
class Stat(dict):

    def __init__(self, db):
    
        super().__init__()
        
        self.db              = db
        self['run']          = 0
        self['last_run']     = 0
        self['alert']        = 0
        self['host']         = 0
        self['return_code']  = []
        self['last_rc']  = []
        
    def count_objects(self):
    
        self['run']         = self.db.fetchall('SELECT COUNT(*) FROM run')[0][0]
        self['last_run']    = self.db.fetchall('SELECT COUNT(*) FROM last_run')[0][0]
        self['alert']       = self.db.fetchall('SELECT COUNT(*) FROM alert')[0][0]
        self['host']        = self.db.fetchall('SELECT COUNT(DISTINCT host) FROM last_run')[0][0]
        self['return_code'] = self.db.fetchall('SELECT return_code, SUM(1) FROM run GROUP BY return_code ORDER BY return_code')
        self['last_rc']     = self.db.fetchall('SELECT return_code, SUM(1) FROM last_run GROUP BY return_code ORDER BY return_code')
    
    def __repr__(self):
        
        ret =  'Total runs     '+str(self['run'])
        ret += '\nLast runs      '+str(self['last_run'])
        ret += '\nAlerts         '+str(self['alert'])
        ret += '\nHosts          '+str(self['host'])
        ret += '\nReturn codes  '
        for rc in self['return_code']:
            
            ret += '\n  '+str(rc[0]).ljust(3)+' '+str(rc[1])
        ret += '\nLast RC       '
        for rc in self['last_rc']:
            
            ret += '\n  '+str(rc[0]).ljust(3)+' '+str(rc[1])
        
        return(ret)
