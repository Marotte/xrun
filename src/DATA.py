def database_sql():

    return("""
    -- xrun Database creation/update
    
    -- Runs
    
    CREATE TABLE IF NOT EXISTS run  (id          INTEGER PRIMARY KEY AUTOINCREMENT,
                                     host        TEXT,
                                     user        TEXT,
                                     return_code INT,
                                     stdout      TEXT,
                                     stderr      TEXT,
                                     exec_status TEXT,
                                     command     TEXT,
                                     start       FLOAT,
                                     end         FLOAT
                                    );

    -- Last runs with distinct status

    CREATE VIEW IF NOT EXISTS last_run AS SELECT DISTINCT id, host, user, command, return_code, stdout, stderr, exec_status, start, end, MAX(start)
                                          FROM run GROUP BY host, command
                                          ORDER BY MAX(start) DESC;
                                          
                                          
    -- Alerts
    
    CREATE TABLE IF NOT EXISTS alert   (host             TEXT,
                                        command          TEXT,
                                        line             TEXT,
                                        exec_status      TEXT,
                                        check_str        TEXT,
                                        first_time       FLOAT,
                                        update_time      FLOAT,
                                        severity         INT,
                                        PRIMARY KEY(host, command)
                                        );
                                        
    """)


