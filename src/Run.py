import datetime

class Run:

    def __init__(self, host = '', user = '', return_code = -1, stdout = '', stderr = '', exec_status = '', command = '', start = 0, end = 0):
    
        self.host        = host
        self.user        = user
        self.return_code = return_code
        self.stdout      = stdout
        self.stderr      = stderr
        self.exec_status = exec_status
        self.command     = command
        self.start       = start
        self.end         = end

    def local_time(self, t):
        
        return(datetime.datetime.utcfromtimestamp(t).replace(tzinfo=datetime.timezone.utc).astimezone(tz=None).strftime('%Y-%m-%d %H:%M:%S'))

    def duration(self):
        
        return(round(self.end - self.start, 3))

    def __getitem__(self, i):
        
        return((self.host, self.user, self.return_code, self.stdout, self.stderr, self.exec_status, self.command, self.start, self.end)[i])
 
    def __repr__(self):

        command = self.command.encode('unicode-escape').decode()
        
        start_time = self.local_time(self.start)
        
        if self.return_code == 0:
            
            output = self.stdout
            
            if output == []:
                
                output = self.stderr
            
            return('OK:'.ljust(6)+' '+self.user+'@'+self.host.ljust(15)+' \''+command+'\' '+str(output)+' '+start_time+' '+str(self.duration()))
        
        elif self.return_code == -1:
            
            return('NORUN:'.ljust(6)+' '+self.user+'@'+self.host.ljust(15)+' \''+command+'\' '+str(self.exec_status)+'\t'+start_time+' '+str(self.duration()))

        else:
            
            return('ERROR:'.ljust(6)+' '+self.user+'@'+self.host.ljust(15)+' \''+command+'\' '+str(self.stdout)+' '+str(self.stderr)+' '+start_time+' '+str(self.duration()))

    def store(self, runs = [], db = None):
        
        if not db or type(runs) is not list:
            
            return(False)
        
        _runs = []
            
        for r in runs:
            
            stdout = ''.join(r[3])
            stderr = ''.join(r[4])
            
            _runs.append((r[0],r[1],r[2],stdout,stderr,r[5],r[6],r[7],r[8]))
            
        db.cursor.executemany('INSERT OR IGNORE INTO run (host, user, return_code, stdout, stderr, exec_status, command, start, end) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', _runs)
        db.commit()

        return(_runs)         

    def last_run(self, db):
        
        runs = []
        last_run = db.fetchall('SELECT host, user, return_code, stdout, stderr, exec_status, command, start, end FROM last_run')
        
        for r in last_run:
        
            stdout = r[3].split('\n')
            stderr = r[4].split('\n')
            

            runs.append((r[0], r[1], r[2], stdout, stderr, r[5], r[6], r[7], r[8]))
            
        return(runs)    

    def last(self, db, where_clause = ('return_code', 0, True)):
        
        if where_clause[2]:
        
            query = 'SELECT * FROM last_run WHERE '+where_clause[0]+' = '+str(where_clause[1])
            
        else:
            
            query = 'SELECT * FROM last_run WHERE '+where_clause[0]+' <> '+str(where_clause[1])    
        
        ret = ''
        
        for run in db.fetchall(query):
        
            ret += str(run[0])+' '+run[2]+'@'+run[1]+': '+run[3]+'\n'
            ret += run[5]+'\n'
        
        return(ret)


        
