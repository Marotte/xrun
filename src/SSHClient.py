import sys, os, socket, binascii

try:

    import paramiko, queue

    from multiprocessing import Process, Queue, Pool

    from time import sleep, time
    from Crypto.PublicKey import RSA

    import Logger, Run

except ImportError as e:
    
    print(str(e), file=sys.stdout)
    sys.exit(1)

class SSHClient:

    def __init__(self, privkeyfile = '', ssh_delay = 0.5):
    
        self.logger = Logger.Logger(0)
    
        try:
            
            self.key             = paramiko.RSAKey.from_private_key(open(privkeyfile, 'r'))
            self.timeout         = 5
            self.banner_timeout  = 5
            self.ssh_delay       = ssh_delay
            self.channel_timeout = 30
            self.queue_timeout   = 30

            
            self.logger.log(-1, 'Using key: '+binascii.hexlify(self.key.get_fingerprint()).decode('utf8'))
            
        except FileNotFoundError as e:

            self.logger.log(3, str(e))
            
    def create_new_rsa_key(self, filename = 'xrun_id'):
        
        key = RSA.generate(2048, os.urandom)
        private = key.exportKey('PEM')
        #~ public  = key.publickey().exportKey('PEM')
        ssh     = key.exportKey('OpenSSH')
        open(filename, 'wb').write(private)
        open(filename+'.pub', 'wb').write(ssh)
        
    def execute(self, command, host, user, q):
        """Execute command on host."""
        
        start  = time()
        try:
            
            self.logger.log(0, user+'@'+host+' '+command)
            exec_status = ''
            return_code = -1
            
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.load_system_host_keys()
            self.client.connect(host, username=user, password='', pkey=self.key, timeout=self.timeout, banner_timeout=self.banner_timeout)
            
            std    = self.client.exec_command(command, timeout=self.channel_timeout)
            end    = time()
            stdout = list(std[1])
            stderr = list(std[2])
            return_code = std[1].channel.recv_exit_status()
            q.put((host, user, return_code, stdout, stderr, exec_status, command, start, end))
            self.client.close()

        except (paramiko.ssh_exception.AuthenticationException,
                paramiko.ssh_exception.NoValidConnectionsError,
                paramiko.ssh_exception.SSHException,
                OSError, EOFError, ConnectionResetError) as e:

            self.logger.log(-1, str(e))
            exec_status = str(e)
            end    = time()
            q.put((host, user, -1, [], [], exec_status, command, start, end))
            self.client.close()

    def execute_job(self, filename, host, user, qj):
        """Copy file to remote host, execute it and delete it."""
        
        try:
            
            self.logger.log(0, user+'@'+host+' '+filename)
            exec_status = ''
            return_code = -1
            start  = time()
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.load_system_host_keys()
            self.client.connect(host, username=user, password='', pkey=self.key, timeout=self.timeout, banner_timeout=self.banner_timeout)
            
            sftp = self.client.open_sftp()

            remote_filename = filename.replace('/','_')
                
            sftp.put(filename, remote_filename, confirm=True)
            sftp.chmod(remote_filename, 750)

            std    = self.client.exec_command('./'+remote_filename, timeout=self.channel_timeout)
            end    = time()
            stdout = list(std[1])
            stderr = list(std[2])
            return_code = std[1].channel.recv_exit_status()
            qj.put((host, user, return_code, stdout, stderr, exec_status, filename, start, end))
            sftp.remove(remote_filename)
            sftp.close()
            self.client.close()

        except (paramiko.ssh_exception.AuthenticationException,
                paramiko.ssh_exception.NoValidConnectionsError,
                paramiko.ssh_exception.SSHException,
                OSError, EOFError, ConnectionResetError,
                FileNotFoundError) as e:

            self.logger.log(-1, str(e))
            exec_status = str(e)
            end    = time()
            qj.put((host, user, -1, [], [], exec_status, filename, start, end))
            self.client.close()

    def execute_cmds(self, commands, host, user):
        """Execute several commands on host in parallel."""
        
        q = Queue()
        runs = []

        for cmd in commands:
        
            if not cmd:
                
                continue
        
            Process(target=self.execute, args=(cmd, host, user, q)).start()
            sleep(self.ssh_delay) # Wait a bit before making another SSH connection to the same host…

        for cmd in commands:
            
            _start = time()
            
            try:
            
                runs.append(Run.Run(*q.get(self.queue_timeout)))

                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)

                run.append(Run.Run(host, user, -2, [], [], 'Command didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass    

        return(runs)    

    def execute_jobs(self, filenames, host, user):
        """Execute several jobs on host in parallel."""
        
        jq = Queue()
        runs = []

        for job in filenames:
        
            if not job:
                
                continue
        
            Process(target=self.execute_job, args=(job, host, user, jq)).start()

        for job in filenames:
            
            _start = time()
            
            try:

                runs.append(Run.Run(*jq.get(timeout=self.queue_timeout)))
                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)
                run.append(Run.Run(host, user, -2, [], [], 'Job didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass    

        return(runs)            

    def execute_hosts(self, hosts, command, user):
        """Execute a command on several hosts in parallel."""
        
        q = Queue()
        runs = []

        for host in hosts:
        
            if not host:
                
                continue
        
            Process(target=self.execute, args=(command, host, user, q)).start()

        for host in hosts:
            

            _start = time()

            try:
            
                runs.append(Run.Run(*q.get(timeout=self.queue_timeout)))
                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)

                run.append(Run.Run(host, user, -2, [], [], 'Command didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass    
 
        return(runs)    

    def execute_chain(self, commands, host, user):
        """Execute several commands on host one by one."""
        
        q = Queue()
        runs = []

        for cmd in commands:
        
            if not cmd:
                
                continue
        
            self.execute(cmd, host, user, q)

        for cmd in commands:

            _start = time()

            try:
            
                runs.append(Run.Run(*q.get(timeout=self.queue_timeout)))
                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)

                run.append(Run.Run(host, user, -2, [], [], 'Command didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass    

        return(runs)   

    def execute_job_hosts(self, hosts, job, user):
        """Execute a command on several hosts in parallel."""
        
        q = Queue()
        runs = []

        for host in hosts:
        
            if not host:
                
                continue
        
            Process(target=self.execute_job, args=(job, host, user, q)).start()

        for host in hosts:
            

            _start = time()

            try:
            
                runs.append(Run.Run(*q.get(timeout=self.queue_timeout)))
                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)

                run.append(Run.Run(host, user, -2, [], [], 'Job didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass
 
        return(runs)    

    def execute_job_chain(self, jobs, host, user):
        """Execute several commands on host one by one."""
        
        q = Queue()
        runs = []

        for job in jobs:
        
            if not job:
                
                continue
        
            self.execute_job(job, host, user, q)

        for job in jobs:
            

            _start = time()

            try:
            
                runs.append(Run.Run(*q.get(timeout=self.queue_timeout)))
                
            except(queue.Empty) as e:
                
                print(str(e), file=sys.stderr)

                run.append(Run.Run(host, user, -2, [], [], 'Job didn’t return after '+self.queue_timeout+' seconds', filename, _start, time()))

                pass    

        return(runs)   

    def xrun(self, hosts, commands, user, mode = 'host'):
        
        if not hasattr(self, 'key'):
            
            return([])
        
        runs = []
        
        if mode == 'host':
        
            for h in hosts:
                
                runs += self.execute_cmds(commands, h, user)
            
        elif mode == 'cmd':
        
            for c in commands:
                
                runs += self.execute_hosts(hosts, c, user)

        elif mode == 'chain':
        
            for h in hosts:
                
                runs += self.execute_chain(commands, h, user)

        return(runs)

    def xjob(self, hosts, jobs, user, mode = 'host'):
        
        if not hasattr(self, 'key'):
            
            return([])
        
        runs = []
        
        if mode == 'host':
        
            for h in hosts:
                
                runs += self.execute_jobs(jobs, h, user)
            
        elif mode == 'cmd':
        
            for j in jobs:
                
                runs += self.execute_job_hosts(hosts, j, user)

        elif mode == 'chain':
        
            for h in hosts:
                
                runs += self.execute_job_chain(jobs, h, user)

        return(runs)
