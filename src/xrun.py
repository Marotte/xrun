#!/usr/bin/env python3

import os, sys, argparse, pwd, socket

from pprint import pprint

import Database, Network, SSHClient, Run, DATA, Check, Logger, Stat

if __name__ == '__main__':

    try:
    
        BASENAME  = os.path.basename(sys.argv[0]).split('.')[:-1][0]
        
    except IndexError:
        
        BASENAME  =  os.path.basename(sys.argv[0])
           
    DBFILE    = BASENAME+'.sqlite'
    PRIVKFILE = BASENAME+'_id'
    PUBKFILE  = BASENAME+'_id.pub'
    USER      = pwd.getpwuid(os.getuid()).pw_name
    HOST      = socket.gethostbyname(socket.gethostname())
    SSH_DELAY = 0.1 # Delay between attempting two connections on the same host

    parser       = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-H', '--host', nargs='*', default=[], action='store', help='Host list')
    parser.add_argument('-n', '--network' , nargs='?', action='store', help='Network')

    parser.add_argument('-c', '--command', nargs='*', default=[], action='store', help='Command list')
    parser.add_argument('-j', '--job', nargs='*', default=[], action='store', help='Job list')
    
    parser.add_argument('-m', '--mode', nargs='?', choices=['host', 'cmd', 'chain'], default='host', action='store', help='Execution mode')
    parser.add_argument('-u', '--user', nargs='?', type=str, default=USER, action='store', help='User')
    parser.add_argument('-i', '--ssh-id', nargs='?', type=str, default=PRIVKFILE, action='store', help='Private key file. Default is "xrun_id", the file will be created if it doesn’t exist.')

    parser.add_argument('-Hf', '--host-file', nargs='*', type=str, action='store', help='Host files')
    parser.add_argument('-cf', '--command-file', nargs='*', type=str, action='store', help='Command files')
    parser.add_argument('-jf', '--job-file', nargs='*', type=str, action='store', help='Job files')
    
    parser.add_argument('-a', '--alerts', action='store_true', help='Show alerts')
    parser.add_argument('-l', '--last-runs', action='store_true', help='Show last runs')
    parser.add_argument('-s', '--stat', action='store_true', help='Show database statistics')

    args = parser.parse_args()
    logger = Logger.Logger(0)

    if not os.path.isfile(DBFILE):
    
        db      = Database.Database(DBFILE)
        db.execute_script(DATA.database_sql())
        
    else:
        
        db      = Database.Database(DBFILE)
    
    network  = Network.Network()
    hosts    = args.host + network.addresses(args.network)
    commands = args.command
    jobs     = args.job
    runs     = []
    
    if not os.path.isfile(args.ssh_id):
    
        logger.log(1, 'No RSA ID specified and default key \''+PRIVKFILE+'\' doesn’t exist. Creating the default key.')
        SSHClient.SSHClient.create_new_rsa_key(PRIVKFILE)
        client = SSHClient.SSHClient(PRIVKFILE)
 
    else:
        
        client   = SSHClient.SSHClient(args.ssh_id, SSH_DELAY)

    run_ctrl   = Run.Run()
    check_ctrl = Check.Check()
    stat       = Stat.Stat(db)

    if args.host_file:
        
        for f in args.host_file:
        
            hosts += list(filter(None, open(f, 'r').read().split('\n')))

    if args.command_file:
        
        for f in args.command_file:
        
            commands += list(filter(None, open(f, 'r').read().split('\n')))

    if args.job_file:
        
        for f in args.job_file:
        
            jobs += list(filter(None, open(f, 'r').read().split('\n')))  
    
    if commands:
        
        logger.log(0, 'Running '+str(len(commands))+' commands on '+str(len(hosts))+' hosts')
        runs += client.xrun(hosts, commands, args.user, args.mode)
        
    if jobs:
        
        logger.log(0, 'Running '+str(len(jobs))+' jobs on '+str(len(hosts))+' hosts')
        runs += client.xjob(hosts, jobs, args.user, args.mode)
    
    
    if args.stat:
        
        stat.count_objects()
        print(stat)
       
    #~ print('\n'.join(map(str, runs)))
    run_ctrl.store(runs, db)

    if args.alerts:
    
        # All runs with a negative return code will produce an alert
        # We need at list one filter :
        # Catch all error (rc >= 1) alerts → severity 1 (WARN)
        checks  = [Check.Check(host_filter='.*', command_filter='.*', warn_rc = 1, severity = 1)]

        # More alerts
        # Note that an alert on the 'df' command used like this is not a good idea,
        # as the line’s order may vary.
        # It’s just to illustrate the processing of a multiline output.
        # Lines and fields are numbered from 0.
        #~ checks += [Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, line=1, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, line=2, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, line=3, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, line=4, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='df -Pk \|tail -n \+2', warn_rc=1, line=5, severity=2, value_index=4, max_value=10),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', command_filter='.*meminfo.*Dirty', warn_rc=1, severity=2, value_index=1, max_value=5),
                   #~ Check.Check(host_filter='192.168.0.(45|18|10)', warn_rc=127, severity=999)
                  #~ ]
                  
        runs = run_ctrl.last_run(db)

        alerts = []

        for run in runs:    

            for check in checks:

                alert = check.check(run)

                if alert:
                    
                    alerts.append(alert)
     
        check_ctrl.store_alerts(alerts, db)
        
        print(check_ctrl.list_alerts(db))
    
    if args.last_runs:
        
        for r in run_ctrl.last_run(db):
            
            print(Run.Run(*r))
    
    sys.exit(0)
