import re, time, datetime

class Check(dict):

    def __init__(self, host_filter = '.*', command_filter = '.*', line = 0, split = '\s+', value_index = 0, max_value = float('inf'), min_value = float('-inf'), warn_rc = -1, severity = 1):
    
        super().__init__()
        self['host_filter']    = re.compile(host_filter)
        self['command_filter'] = re.compile(command_filter)
        self['line']           = line
        self['split']          = re.compile(split)
        self['values']         = []
        self['value_index']    = value_index
        self['max_value']      = max_value
        self['min_value']      = min_value
        self['warn_rc']        = warn_rc
        self['severity']       = severity
        
    def __repr__(self):
    
        return(str('CHECK '+str(self['severity'])).ljust(9)+':'+' host="'+self['host_filter'].pattern+'" command="'+self['command_filter'].pattern+'" line='+str(self['line'])+
                                      ' split="'+self['split'].pattern+'" index='+str(self['value_index'])+' min='+str(self['min_value'])+
                                      ' max='+str(self['max_value'])+' rc='+str(self['warn_rc']))
                                      
    def toint(self, string):
        
        if re.match('[0-9]+%', string):
            
            return(int(string.replace('%','')))
            
        elif re.match('[0-9]+', string):
            
            return(int(string)) 

    def check(self, run):
        
        self['run'] = run
        
        try:

            if not (self['command_filter'].match(run[6]) and self['host_filter'].match(run[0])):

                return(None)
            
            self['values'] = self['split'].split(run[3][self['line']])

            rc  = run[2]
            ret = []

            # Always return an alert for negative return codes, severity set to return code.
            if rc < 0:

                return((run[0], run[6], '', run[5], 'RC < 0', time.time(), rc))

            val = self.toint(self['values'][self['value_index']])

            if val:

                if val < self['min_value']:
                     
                    ret += ['L'+str(self['line'])+'F'+str(self['value_index'])+' '+str(val)+' < '+str(self['min_value'])]
                    
                    return((run[0], run[6], ' '.join(self['values']), run[5], '; '.join(ret), time.time(), self['severity']))
                    
                if val > self['max_value']:
                     
                    ret += ['L'+str(self['line'])+'F'+str(self['value_index'])+' '+str(val)+' > '+str(self['max_value'])]
                    
                    return((run[0], run[6], ' '.join(self['values']), run[5], '; '.join(ret), time.time(), self['severity']))    

            if rc >= self['warn_rc']:

                ret += ['RC '+str(rc)+' >= '+str(self['warn_rc'])]

                if not run[5]:
                    
                    info = run[4][0]
                    
                else:
                    
                    info = run[5]    

                return((run[0], run[6], ' '.join(self['values']), info, '; '.join(ret), time.time(), self['severity']))

        except (TypeError, IndexError, ValueError) as e:

            return(False)


    def store_alerts(self, alerts = [], db = None):
        
        if not db or type(alerts) is not list:
            
            return(False)

        db.cursor.executemany('INSERT OR IGNORE INTO alert (host, command, line, exec_status, check_str, first_time, severity) VALUES (?, ?, ?, ?, ?, ?, ?)', alerts)
        db.commit()
        for a in alerts:

            db.cursor.execute('UPDATE alert SET line = ?, update_time = ?, check_str = ?, exec_status = ?, severity = ? WHERE host = ? AND command = ?', (a[2],a[5],a[4],a[3],a[6],a[0],a[1]))
        
        db.commit()
        return(alerts)     

    def get_alerts(self, db = None):
        
        qry = 'SELECT *, CAST((update_time - first_time) AS INT) AS duration FROM alert ORDER BY duration ASC, severity DESC'
        res = db.fetchall(qry)
        return(res)
        
    def list_alerts(self, db = None):
        
        data = self.get_alerts(db)
        alerts = []
        for a in data:

            duration = datetime.timedelta(seconds=int(a[8]))
            alerts.append(' '.join([str(a[0]), str(a[1]), str(a[2]), str(a[3]), str(a[4]), str(duration), str(a[7])]))
            
        return('\n'.join(alerts))    
