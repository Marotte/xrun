
import ipaddress

class Network(dict):

    def __init__(self):

        super().__init__()
        self['netname'] = ''
        self['name']    = ''
        
    def hosts(self, netname):
        
        try:
            
            hosts = list(ipaddress.IPv4Network(netname).hosts())
        
        except ipaddress.AddressValueError:
            
            return([])
        
        return(hosts)

    def addresses(self, netname):
        
        addresses = []
        
        for a in self.hosts(netname):
            
            addresses.append(a.exploded)
            
        return(addresses)
        

        
